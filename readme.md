## 说明

为开源CLup准备的Python环境

如果是Redhat7.X或CentOS7.X请下载发行版本中的：
* csupy3.9.16.el7.tar.xz

如果是Redhat8.X或CentO8.X或RockyLinux 8.X请下载发行版本中的：
* csupy3.9.16.el8.tar.xz
